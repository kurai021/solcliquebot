command list and how to use it:

Note:All commands must be called in this way "cliq!command-name".

Admin commands:
kick: Kicks a user from the server.
example: cliq!kick @user

Music commands:
Note: you should be within a voice channel

play: Find and play a song from youtube, if a song is already playing, will be added to the playlist.
example: cliq!play name-song

pause: Pause the current song
example: cliq!pause

resume: Resume the current song
example: cliq!resume

stop: Stop the current playlist
example: cliq!stop

skip: Skip the current song and play the next one
example: cliq!skip

volume: Change the volume of the current song
example: cliq!volume (0-100)

bassboost: Basboost filter of the current song
example: cliq!bassboost

normalize: Normalize filter of the current song
example: cliq!normalize

eightball: Ask the magic 8ball a question
example: cliq!eightball question

flipcoin: Flip a coin
example: cliq!flipcoin

rsp: Play rock, paper, scissors with the bot
example: cliq!rsp rock

horoscope: Get your daily horoscope
example: cliq!horoscope your-sign

coingecko: Get the price of a coin
example: cliq!coingecko coin-name

pokedex: Get the information of a pokemon
example: cliq!pokedex pokemon-name

pkred: Play Pokemon Red in pokemon-red channel (if exist)
command list: cliq!pkred play - boot the game
cliq!pkred up - press up
cliq!pkred down - press down
cliq!pkred left - press left
cliq!pkred right - press right
cliq!pkred a - press A
cliq!pkred b - press B
cliq!pkred start - press start
cliq!pkred select - press select

listcommands: List all the available commands
example: cliq!listcommands

help: Get help about a command
example: cliq!help command-name
