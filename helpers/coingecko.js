const { CoinGeckoClient } = require("coingecko-api-v3")
const util = require("../util/utils")

const client = new CoinGeckoClient({
    timeout: 10000,
    autoRetry: true,
});

function getInfo(args, message) {
    const input = {
        vs_currencies: 'USD',
        ids: args,
        include_market_cap: true,
        include_24hr_vol: true,
        include_24hr_change: true,
        include_last_updated_at: true,
    }

    client.simplePrice(input)
        .then(result => {
            console.log(result);

            message.channel.send({
                embeds: [
                    {
                        color: 'F70094',
                        title: `${util.capitalize(args)} price:`,
                        fields: [
                            {
                                name: "Price",
                                value: `${result[args].usd} USD`
                            },
                            {
                                name: "USD Market Cap",
                                value: `${result[args].usd_market_cap} USD`
                            },
                            {
                                name: "24 Hours Volume",
                                value: `${result[args].usd_24h_vol} USD`
                            },
                            {
                                name: "24 Hours Change",
                                value: `${util.round(result[args].usd_24h_change)}%`
                            }
                        ]
                    }
                ]
            })
        })
        .catch(error => {
            message.reply("There was an error during the search process")
        })
}

module.exports = {
    getInfo: getInfo
}