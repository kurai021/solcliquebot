const { MessageAttachment } = require('discord.js');
const gameboy = require('serverboy');
const Canvas = require('canvas');
const fs = require('fs');

const rom = fs.readFileSync('./helpers/roms/pkred.gb');
const gb_instance = new gameboy()

gb_instance.loadRom(rom);

let frames = 0;
let repeat;
let currentScreen

function play(message) {

    currentScreen = gb_instance.doFrame();

    frames++;

    if (frames === 120) {
        replyFrame(currentScreen, message);
        frames = 0;
    }

    repeat = setTimeout(() => {
        play(message);
    }, 1000 / 60);
}

function stop(message){
    if(repeat !== undefined){
        clearTimeout(repeat);
        repeat = undefined;
        message.reply('Emulation temporarily stopped.');
        console.log(repeat);
    }
    else {
        message.reply('Emulation is not running.');
    }
}

function replyFrame(currentScreen, message) {
    const canvas = Canvas.createCanvas(160, 144);
    const context = canvas.getContext('2d');
    const ctx_data = context.createImageData(160, 144);

    for (let i = 0; i < currentScreen.length; i++) {
        ctx_data.data[i] = currentScreen[i];
    }

    context.putImageData(ctx_data, 0, 0);

    const pkredAttachment = new MessageAttachment(canvas.toBuffer(), 'pkred.png');

    message.reply({ files: [pkredAttachment] })

}

function up(message){
    if(repeat !== undefined){
        gb_instance.pressKey(gameboy.KEYMAP.UP);
    }
    else {
        message.reply('Emulation is not running.');
    }
}

function down(message){
    if(repeat !== undefined){
        gb_instance.pressKey(gameboy.KEYMAP.DOWN);
    }
    else {
        message.reply('Emulation is not running.');
    }
}

function left(message){
    if(repeat !== undefined){
        gb_instance.pressKey(gameboy.KEYMAP.LEFT);
    }
    else {
        message.reply('Emulation is not running.');
    }
}

function right(message){
    if(repeat !== undefined){
        gb_instance.pressKey(gameboy.KEYMAP.RIGHT);
    }
    else {
        message.reply('Emulation is not running.');
    }
}

function a(message){
    if(repeat !== undefined){
        gb_instance.pressKey(gameboy.KEYMAP.A);
    }
    else {
        message.reply('Emulation is not running.');
    }
}

function b(message){
    if(repeat !== undefined){
        gb_instance.pressKey(gameboy.KEYMAP.B);
    }
    else {
        message.reply('Emulation is not running.');
    }
}

function select(message){
    if(repeat !== undefined){
        gb_instance.pressKey(gameboy.KEYMAP.SELECT);
    }
    else {
        message.reply('Emulation is not running.');
    }
}

function start(message){
    if(repeat !== undefined){
        gb_instance.pressKey(gameboy.KEYMAP.START);
    }
    else {
        message.reply('Emulation is not running.');
    }
}

module.exports = {
    play: play,
    stop: stop,
    up:up,
    down:down,
    left:left,
    right:right,
    a:a,
    b:b,
    select:select,
    start:start
}