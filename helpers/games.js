function eightBall(){
    const eightBallResponses =  ["In my opinion, yes", "It's true", "It's decidedly so", "Probably", "Good prognosis", "Everything points to yes", "No doubt", "Yes",
    "Yes, definitely", "You must trust it", "Vague answer, try again", "Ask another time", "I'd better not tell you now", "I can't predict it now",
    "Concentrate and ask again", "Maybe", "Don't count on it", "My answer is no", "My sources tell me no", "The prospects are not good", "Very doubtful"];
    const randomNumber = Math.random();
    const randomAnswer = Math.floor(randomNumber * 21);
    const answer = eightBallResponses[randomAnswer];

    return answer;

}

function flipCoin(){
    const coinSides = ["head","tails"];
    const randomNumber = Math.random();
    const randomSide = Math.floor(randomNumber * coinSides.length);
    const answer = coinSides[randomSide];

    return answer
}

function rsp(data){
    const rspResponses = ["rock","scissors","paper"];
    
    if(rspResponses.includes(data)){
        const randomNumber = Math.random();
        const randomRSP = Math.floor(randomNumber * 3);
        const answer = rspResponses[randomRSP];

        if(data == answer){
            return `You choose: ${data} and I choose: ${answer} **Draw!**`
        }

        else if(data == 'paper' && answer == 'scissors'){
            return `You choose: ${data} and I choose: ${answer} **You lost!**`
        }

        else if(data == 'scissors' && answer == 'rock'){
            return `You choose: ${data} and I choose: ${answer} **You lost!**`
        }

        else if(data == 'rock' && answer == 'paper'){
            return `You choose: ${data} and I choose: ${answer} **You lost!**`
        }

        else if(data == 'scissors' && answer == 'paper'){
            return `You choose: ${data} and I choose: ${answer} **You win!**`
        }

        else if(data == 'paper' && answer == 'rock'){
            return `You choose: ${data} and I choose: ${answer} **You win!**`
        }

        else {
            return `You choose: ${data} and I choose: ${answer} **You win!**`
        }

    }

    else {
        return "The options are rock, scissors and paper"
    }
}

module.exports = {
    eightBall:eightBall,
    flipCoin:flipCoin,
    rsp:rsp
}