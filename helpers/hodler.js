const solRayz = require('@nfteyez/sol-rayz');

async function verifyHodler(message, user, address, kingclique, teamclique, teamcaptain) {
    const tokenList = await solRayz.getParsedNftAccountsByOwner({
        publicAddress: address,
        sanitize: true,
        strictNftStandard: true,
        stringifyPubKeys: true,
        sort: true
    })

    let counter = 0;
    const regex = /SolClique (.*)/i;

    tokenList.forEach(token => {
        if (token.data.name.match(regex)) {
            counter++;
        }

    });

    if (user) {
        if (message.partial) await message.fetch()
        if (message.partial) await message.fetch()
        if (user.bot) return;
        if (!message.guild) return;

        switch (true) {
            case counter >= 1 && counter <= 4:
                await message.guild.members.cache.get(user.id).roles.add(teamclique);
                await message.reply(`@${user.username} has access to #verified-hodlers.`);
                break;
            case counter >= 5 && counter <= 29:
                await message.guild.members.cache.get(user.id).roles.add(teamcaptain);
                await message.reply(`@${user.username} has access to #verified-hodlers.`);
                break;
            case counter >= 30:
                await message.guild.members.cache.get(user.id).roles.add(kingclique);
                await message.reply(`@${user.username} has access to #verified-hodlers.`);
                break;
            default:
                await message.guild.members.cache.get(user.id).roles.remove(kingclique);
                await message.guild.members.cache.get(user.id).roles.remove(teamclique);
                await message.guild.members.cache.get(user.id).roles.remove(teamcaptain);
                await message.reply(`@${user.username} is not a hodler, does not have access to #verified-hodlers.`);
                break;
        }
    }
    else {
        await message.reply("User not found.");
    }

}

module.exports = {
    verifyHodler: verifyHodler
}