const fetch = require('node-fetch');

function getHoroscope(data){
    const signs = ["aries", "taurus", "gemini", "cancer", "leo", "virgo", "libra", "scorpio", "sagittarius", "capricorn", "aquarius", "pisces"]
    let url;

    if(signs.includes(data)){
        switch(data){
            case 'aries':
                url = `https://aztro.sameerkumar.website/?sign=aries&day=today`;
                break;
            case 'tauro':
                url = `https://aztro.sameerkumar.website/?sign=taurus&day=today`;
                break;
            case 'geminis':
                url = `https://aztro.sameerkumar.website/?sign=gemini&day=today`;
                break;
            case 'géminis':
                url = `https://aztro.sameerkumar.website/?sign=gemini&day=today`;
                break;
            case 'cancer':
                url = `https://aztro.sameerkumar.website/?sign=cancer&day=today`;
                break;
            case 'leo':
                url = `https://aztro.sameerkumar.website/?sign=leo&day=today`;
                break;
            case 'virgo':
                url = `https://aztro.sameerkumar.website/?sign=virgo&day=today`;
                break;
            case 'libra':
                url = `https://aztro.sameerkumar.website/?sign=libra&day=today`;
                break;
            case 'escorpio':
                url = `https://aztro.sameerkumar.website/?sign=scorpio&day=today`;
                break;
            case 'escorpion':
                url = `https://aztro.sameerkumar.website/?sign=scorpio&day=today`;
                break;
            case 'escorpión':
                url = `https://aztro.sameerkumar.website/?sign=scorpio&day=today`;
                break;
            case 'saggitarius':
                url = `https://aztro.sameerkumar.website/?sign=sagitario&day=today`;
                break;
            case 'capricornio':
                url = `https://aztro.sameerkumar.website/?sign=capricorn&day=today`;
                break;
            case 'acuario':
                url = `https://aztro.sameerkumar.website/?sign=aquarius&day=today`;
                break;
            default:
                url = `https://aztro.sameerkumar.website/?sign=pisces&day=today`;
                break;
        }

        return fetch(url, {
            method: 'POST'
        })
        .then(res => {
            return res.json()
        })
    }
}

module.exports = {
    getHoroscope:getHoroscope
}