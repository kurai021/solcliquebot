const playdl = require("play-dl");

// add the trackStart event so when a song will be played this message will be sent
async function play(args, message, player) {
  if (!message.member.voice.channelId) return await message.reply({ content: "You are not in a voice channel!", ephemeral: true });
  if (message.guild.me.voice.channelId && message.member.voice.channelId !== message.guild.me.voice.channelId) return await message.reply({ content: "You are not in my voice channel!", ephemeral: true });
  const query = args;
  const queue = player.createQueue(message.guild, {
    metadata: {
      channel: message.channel,
    },
    async onBeforeCreateStream(track, source, _queue) {
      // only trap youtube source
      if (source === "youtube") {
        // track here would be youtube track
        return (await playdl.stream(track.url)).stream;
        // we must return readable stream or void (returning void means telling discord-player to look for default extractor)
      }
    }
  });

  // verify vc connection
  try {
    if (!queue.connection) await queue.connect(message.member.voice.channel);
  } catch {
    queue.destroy();
    return await message.reply({ content: "Could not join your voice channel!", ephemeral: true });
  }

  //await message.deferReply();
  const track = await player.search(query, {
    requestedBy: message.user
  }).then(x => x.tracks[0]);

  if (!track) return await message.reply({ content: `❌ | Track **${query}** not found!` });

  queue.play(track);

  return await message.reply({ content: `⏱️ | Loading track **${track.title}**!` });
}

async function volume(args, message, player){
  const queue = player.getQueue(message.guild);
  if (!queue) return await message.reply({ content: "There is no music playing!", ephemeral: true });
  const volume = parseInt(args);
  if (isNaN(volume)) return await message.reply({ content: "Volume must be a number!", ephemeral: true });
  if (volume < 0 || volume > 100) return await message.reply({ content: "Volume must be between 0 and 100!", ephemeral: true });
  queue.volume = volume;
  return await message.reply({ content: `🔉 | Volume set to **${volume}**!` });
}

async function pause(message, player){
  const queue = player.getQueue(message.guild);
  if (!queue) return await message.reply({ content: "There is no music playing!", ephemeral: true });
  queue.setPaused(true);
  return await message.reply({ content: "⏸️ | Paused!" });
}

async function resume(message, player){
  const queue = player.getQueue(message.guild);
  if (!queue) return await message.reply({ content: "There is no music playing!", ephemeral: true });
  queue.setPaused(false);
  return await message.reply({ content: "▶️ | Resumed!" });
}

async function stop(message, player){
  const queue = player.getQueue(message.guild);
  if (!queue) return await message.reply({ content: "There is no music playing!", ephemeral: true });
  queue.destroy()
  return await message.reply({ content: "⏹️ | Stopped!" });
}

async function skip(message, player){
  const queue = player.getQueue(message.guild);
  if (!queue) return await message.reply({ content: "There is no music playing!", ephemeral: true });
  const currentTrack = queue.current;
  const success = queue.skip();
  return await message.reply({ content: success ? `✅ | Skipped **${currentTrack}**!` : "❌ | Something went wrong!" });
}

async function bassboost(message, player){
  const queue = player.getQueue(message.guild);
  if (!queue) return await message.reply({ content: "There is no music playing!", ephemeral: true });
  queue.bassboost = !queue.bassboost;
  return await message.reply({ content: `🎸 | Bassboost set to **${queue.bassboost ? "on" : "off"}**!` });
}

async function normalize(message, player){
  const queue = player.getQueue(message.guild);
  if (!queue) return await message.reply({ content: "There is no music playing!", ephemeral: true });
  queue.normalize = !queue.normalize;
  return await message.reply({ content: `🎸 | Normalize set to **${queue.normalize ? "on" : "off"}**!` });
}

module.exports = {
  play: play,
  volume: volume,
  pause: pause,
  resume: resume,
  stop: stop,
  skip: skip,
  bassboost: bassboost,
  normalize: normalize
}