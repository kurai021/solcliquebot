const Pokedex = require('pokedex-promise-v2')
const util = require("../util/utils")
const P = new Pokedex()

function getInfo(args,message){
    let types = []
    let normalHab = []
    let hiddenHab = []
    let description = [];
    let species = args;
    let pokemon;
    let sprite;

    switch(args){
        case 'deoxys':
            pokemon = 'deoxys-normal';
            sprite = 'deoxys';
            break;
        case 'wormadam':
            pokemon = 'wormadam-plant' || 'wormadam-trash' || 'wormadam-sandy';
            sprite = 'wormadam';
            break;
        case 'giratina':
            pokemon = 'giratina-altered';
            sprite = 'giratina';
            break;
        case 'shaymin':
            pokemon = 'shaymin-land';
            sprite = 'shaymin';
            break;
        case 'basculin':
            pokemon = 'basculin-red-striped' || 'basculin-blue-striped';
            species = 'basculin';
            sprite = 'basculin';
            break;
        case 'darmanitan':
            pokemon = 'darmanitan-standard';
            sprite = 'darmanitan';
            break;
        case 'tornadus':
            pokemon = 'tornadus-incarnate';
            sprite = 'tornadus';
            break;
        case 'landorus':
            pokemon = 'landorus-incarnate';
            sprite = 'landorus';
            break;
        case 'thundurus':
            pokemon = 'thundurus-incarnate';
            sprite = 'thundurus';
            break;
        case 'keldeo':
            pokemon = 'keldeo-ordinary';
            sprite = 'keldeo';
            break;
        case 'meloetta':
            pokemon = 'meloetta-aria';
            sprite = 'meloetta';
            break;
        case 'aegislash':
            pokemon = 'aegislash-shield';
            sprite = 'aegislash';
            break;
        case 'pumpkaboo':
            pokemon = 'pumpkaboo-average';
            sprite = 'pumpkaboo';
            break;
        case 'gourgeist':
            pokemon = 'gourgeist-average';
            sprite = 'gourgeist';
            break;
        case 'mr mime':
            pokemon = 'mr-mime';
            sprite = 'mrmime'
            break;
        case 'mr rime':
            pokemon = 'mr-rime';
            sprite = 'mrrime'
            break;
        case 'mime jr':
            pokemon = 'mime-jr';
            sprite = 'mimejr'
            break;
        case 'oricorio baile':
            species = 'oricorio'
            pokemon = 'oricorio-baile';
            break;
        case 'oricorio pom-pom':
            species = 'oricorio'
            pokemon = 'oricorio-pom-pom';
            sprite = 'oricorio-pompom'
            break;
        case 'oricorio pau':
            species = 'oricorio'
            pokemon = 'oricorio-pau';
            break;
        case 'oricorio sensu':
            species = 'oricorio'
            pokemon = 'oricorio-sensu';
            break;
        case 'lycanroc':
            species = 'lycanroc'
            pokemon = 'lycanroc-midday';
            sprite = 'lycanroc'
            break;
        case 'lycanroc midday':
            species = 'lycanroc'
            pokemon = 'lycanroc-midday';
            sprite = 'lycanroc'
            break;
        case 'lycanroc midnight':
            species = 'lycanroc'
            pokemon = 'lycanroc-midnight';
            break;
        case 'lycanroc dusk':
            species = 'lycanroc'
            pokemon = 'lycanroc-dusk';
            break;
        case 'wishiwashi':
            pokemon = 'wishiwashi-solo';
            sprite = 'wishiwashi';
            break;
        case 'minior':
            pokemon = 'minior-red-meteor' || 'minior-orange-meteor' || 'minior-yellow-meteor' || 'minior-green-meteor' || 'minior-blue-meteor' || 'minior-indigo-meteor' || 'minior-violet-meteor';
            sprite = 'minior';
            break;
        case 'mimikyu':
            pokemon = 'mimikyu-disguised';
            sprite = 'mimikyu';
            break;
        default:
            pokemon = args
            break;
    }

    if(sprite == null){
        sprite = pokemon
    }

    P.getPokemonByName(pokemon)
        .then(async function(res){

            res.types.forEach(item => {
                types.push(item.type.name)
            })

            res.abilities.forEach(item => {
                if(item.is_hidden == false){
                    normalHab.push(item.ability.name)
                }
                else {
                    hiddenHab.push(item.ability.name)
                }
            })

            P.getPokemonSpeciesByName(species)
                .then(function(res){
                    res.flavor_text_entries.forEach(function(item){
                        if(item.language.name == "en"){
                            description.push(item.flavor_text);
                        }
                    })

                    if(hiddenHab.length == 0){
                        hiddenHab.push("No hidden skills")
                    }

                    message.channel.send({embeds: [
                        {
                            color: 'F70094',
                            title: `Name: ${util.capitalize(args)}`,
                            description:`**Description:** ${description[0]}`,
                            fields: [
                                {
                                    name: "Type",
                                    value: `${types.join(" / ")}`
                                },
                                {
                                    name: "Normal Skills",
                                    value: `${normalHab}`
                                },
                                {
                                    name: "Hidden Skills",
                                    value: `${hiddenHab}`
                                }
                            ],
                            thumbnail: {
                                url: `https://play.pokemonshowdown.com/sprites/ani/${sprite}.gif`
                            }
                        }
                    ]})

                })
                .catch(function(err){
                    message.reply("There was an error during the search process")
                    console.log("There was an error:", err)
                })

        })
        .catch(function(err){
            message.reply("The search could not be performed or perhaps the name is incorrect.")
            console.log(err)
        })
}

module.exports = {
    getInfo:getInfo
}