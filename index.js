//const dotenv = require('dotenv');
//dotenv.config();

const Discord = require('discord.js');
const { Player } = require("discord-player");

const hodler = require('./helpers/hodler.js');
const games = require("./helpers/games")
const horoscope = require('./helpers/horoscope')
const poke = require("./helpers/pokedex")
const coingecko = require("./helpers/coingecko")

const help = require("./util/help.json")
const counter = require("./util/userCounter")

const client = new Discord.Client({ intents: ["GUILDS", "GUILD_MESSAGES", 'GUILD_VOICE_STATES', 'GUILD_MEMBERS'] })
const player = new Player(client);

const musicplayer = require("./helpers/musicplayer")
const gb = require("./helpers/gameboy")

player.on("trackStart", (queue, track) => queue.metadata.channel.send(`🎶 | Now playing **${track.title}**!`))

const prefix = "cliq!"

client.on('ready', () => {
    console.log('I am ready!');
});

client.on('guildMemberAdd', member => {
    const channel = member.guild.channels.cache.find(ch => ch.name === 'general-chat💬');

    if (!channel) return;
    counter.userCounter(member);
    channel.send(`Welcome to SolClique, ${member} cliq cliq!`);
});

client.on('guildMemberRemove', member => {
    counter.userCounter(member);
})

client.on('messageCreate', message => {
    if (message.author.bot) return;
    if (!message.content.startsWith(prefix)) return;

    const commandBody = message.content.slice(prefix.length)
    const args = commandBody.trim().split(/ +/g)
    const command = args.shift().toLowerCase()
    const argsMerged = args.join(' ')

    const user = message.mentions.users.first()
    const existingMember = message.guild.members.fetch(user)

    if (command === "kick") {
        if(message.member.permissions.has("KICK_MEMBERS")) {
            if (user) {
                if (existingMember) {
                    existingMember.kick()
                        .then(() => {
                            message.channel.send(`${user.tag} has been kicked`)
                        })
                        .catch(err => {
                            message.reply("The member could not be kicked")
                            console.log(err)
                        })
                }
    
                else {
                    message.reply("The user does not exist in the server!")
                }
            }
    
            else {
                message.reply("You did not mention a user to kick!")
            }
        }
        else {
            message.reply("You do not have permission to kick members!")
        }
    }

    else if (command === "hodler") {
        if(message.member.permissions.has('MANAGE_ROLES')) {
            if(message.channel.name.includes("✅-verify-submission")){
                if(!args.length){
                    message.reply("You did not specify an address!")
                }
    
                else {
                    hodler.verifyHodler(message, user, args[1], process.env.KING_CLIQUE_ROLE, process.env.TEAM_CLIQUE_ROLE, process.env.TEAM_CAPTAIN_ROLE)
                }
            }
            else {
                message.reply("You must be in ✅-verify-submission channel to use this command!")
            }
        }
        else {
            message.reply("You do not have permission to use this command!")
        }
    }

    else if (command === "eightball") {
        if (!args.length) {
            message.reply("You did not ask any questions")
        }

        else {
            const response = games.eightBall()
            message.reply(response)
        }
    }

    else if (command === "flipcoin") {
        const response = games.flipCoin()
        message.reply(response)
    }

    else if (command === "rsp") {
        if (!args.length) {
            message.reply("You did not choose any option")
        }

        else {
            const response = games.rsp(argsMerged)
            message.reply(response)
        }
    }

    else if (command === "horoscope") {
        if (!args.length) {
            message.reply("You did not include your zodiac sign")
        }

        else {
            horoscope.getHoroscope(argsMerged)
                .then(async res => {
                    await message.reply(`${res.description}\n\nYour lucky number is: ${res.lucky_number}`, { split: { char: ' ' } })
                })
        }
    }

    else if (command == "play") {
        if (!args.length) {
            message.reply("You did not include any song")
        }

        else {
            musicplayer.play(argsMerged, message, player)
        }
    }

    else if (command == "volume") {
        if (!args.length) {
            message.reply("You must specify the volume level")
        }

        else {
            musicplayer.volume(argsMerged, message, player)
        }
    }

    else if (command == "pause") {
        musicplayer.pause(message, player)
    }

    else if (command == "resume") {
        musicplayer.resume(message, player)
    }

    else if (command == "stop") {
        musicplayer.stop(message, player)
    }

    else if (command == "skip") {
        musicplayer.skip(message, player)
    }

    else if (command == "bassboost") {
        musicplayer.bassboost(message, player)
    }

    else if(command == "normalize") {
        musicplayer.normalize(message, player)
    }

    else if (command == "pkred") {
        if(message.channel.name.includes("🌪-spam-channel")){
            if (args[0] == "play") {
                gb.play(message)
            }
            else if(args[0] == "stop") {
                gb.stop(message)
            }
            else if(args[0] == "up") {
                gb.up(message)
            }
            else if(args[0] == "down") {
                gb.down(message)
            }
            else if(args[0] == "left") {
                gb.left(message)
            }
            else if(args[0] == "right") {
                gb.right(message)
            }
            else if(args[0] == "a") {
                gb.a(message)
            }
            else if(args[0] == "b") {
                gb.b(message)
            }
            else if(args[0] == "select") {
                gb.select(message)
            }
            else if(args[0] == "start") {
                gb.start(message)
            }
            else {
                message.reply("This command doesn't correspond to any button")
            }
        }
        else {
            message.reply("You can only use this command in 🌪-spam-channel")
        }
    }

    else if (command === "pokedex") {
        if (!args.length) {
            message.reply("You have not included a pokemon to search for")
        }

        else {
            poke.getInfo(argsMerged, message)
        }
    }

    else if (command === "coingecko") {
        if (!args.length) {
            message.reply("You have not included a coin or token to search for")
        }

        else {
            coingecko.getInfo(argsMerged, message)
        }
    }

    else if (command === "listcommands") {
        message.channel.send({
            embeds: [
                {
                    color: 'F70094',
                    title: "SolCliqueBot commands",
                    description: 'Hi, I am SolCliqueBot (˵◕ ɜ◕˵) and here is my list of commands. You can see a more detailed description of each command separately with `cliq!help command-name`.',
                    fields: [
                        {
                            name: "Admin commands",
                            value: '`kick` `hodler`'
                        },
                        {
                            name: "Music commands",
                            value: '`play` `pause` `volume` `stop` `skip` `bassboost` `normalize`'
                        },
                        {
                            name: "Fun commands",
                            value: '`eightball` `flipcoin` `rsp` `horoscope` `pkred`'
                        },
                        {
                            name: "Search commands",
                            value: '`pokedex` `coingecko`'
                        },
                        {
                            name: "Other commands",
                            value: '`listcommands` `help`'
                        }
                    ]
                }
            ]
        })
    }

    else if (command === "help") {
        if (!args.length) {
            message.reply("You did not include a command to search")
        }
        else {
            if (help.commands.some(command => command.name === argsMerged)) {
                const helpArr = help.commands.find(obj => {
                    return obj.name === argsMerged
                })

                const title = helpArr.name
                const description = helpArr.description
                const example = helpArr.example

                message.channel.send({
                    embeds: [
                        {
                            color: 'F70094',
                            title: `${title}`,
                            description: `${description}`,
                            fields: [
                                {
                                    name: "Example of the use of this command",
                                    value: `${example}`
                                }
                            ]
                        }
                    ]
                })
            }
            else {
                message.reply("I can't find the command")
            }
        }
    }

    else {
        message.reply(`Command not found`)
    }

})

client.login(process.env.BOT_TOKEN);