function userCounter(member){
    const userCount = member.guild.memberCount;
    const regex = "👤Total members:";
    let userCountChannel = member.guild.channels.cache.filter(channel => (new RegExp(regex)).test(channel.name)).first();

    if(userCountChannel){
        userCountChannel.setName(`👤Total members: ${userCount}`);
    }
}

module.exports = {
    userCounter: userCounter
}