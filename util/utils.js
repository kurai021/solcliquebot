function capitalize(s) {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

function round(num){
    return Math.round((num + Number.EPSILON) * 100) / 100
}

module.exports = {
    capitalize:capitalize,
    round:round
}